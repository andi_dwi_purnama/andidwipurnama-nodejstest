const yup = require('yup')
const xyup = require('./xyup')

const dict = {
  id: {
    required: {
      name: 'Name is required',
    },
  },
}

const getShapeSchema = (required, language) => {
  // Default Langauge Id (Indonesia)
  const msg = Object.assign(dict.id, dict[language])
  return {
    id: xyup.uuid('Invalid Id', required),
    name: yup.string().required(msg.required.name),
  }
}

module.exports = xyup.generateFormSchema(getShapeSchema)
