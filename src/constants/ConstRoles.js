const ID_ADMIN = 'admin'
const ID_GUEST = 'general'

module.exports = {
  ID_ADMIN,
  ID_GUEST,
}
